package com.pami.group2.fitnesstrackersensormodules;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DatabasesQueries extends AppCompatActivity {

    EditText heightText, weightText;
    Button saveBtn;
    DatabaseReference db;
    User user;
    long userId = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.profile);

        heightText = (EditText)findViewById(R.id.heightText);
        weightText = (EditText)findViewById(R.id.weightText);

        saveBtn = (Button)findViewById(R.id.saveBtn);
        db = FirebaseDatabase.getInstance().getReference().child("users");
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                    userId = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        user = new User();
//        user.setHeight(heightText);

        saveBtn.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                float height = Float.parseFloat(heightText.getText().toString().trim());
                float weight = Float.parseFloat(weightText.getText().toString().trim());

                user.setHeight(height);
                user.setWeight(weight);
               // db.push().setValue(user);

                db.child(String.valueOf(userId+1)).setValue(user);
                //db.push().setValue(user);

                Toast.makeText(DatabasesQueries.this, "Inserted successfully!", Toast.LENGTH_SHORT).show();
            }
        });


        retrieveDataFromDatabase();
    }

    public void retrieveDataFromDatabase(){
        db = FirebaseDatabase.getInstance().getReference().child("users").child("2");
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    String height = dataSnapshot.child("height").getValue().toString();
                    Log.i("height of user with id 2 : ",  height);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
