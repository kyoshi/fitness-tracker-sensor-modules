package com.pami.group2.fitnesstrackersensormodules;

import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.fitness.result.DataReadResult;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class StepCounter extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private BottomNavigationView navView;
    private static final int REQUEST_OAUTH = 1;
    private static final String AUTH_PENDING = "auth_state_pending";
    private boolean authInProgress = false;
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_counter);
        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        updateNavBar();

        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }

        initGoogleClient();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startIntent(MainActivity.class);
                    return true;
                case R.id.navigation_step_rt:
                    startIntent(StepCounterRTActivity.class);
                    return true;
                case R.id.navigation_step_daily:
                    return true;
            }
            return false;
        }
    };

    private void startIntent(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.client.connect();
    }

    public void onConnected(Bundle b) {
        new FetchDailyStep().execute();
        new FetchStepByDate().execute();
        new FetchWeeklyStep().execute();
        new FetchStepByActivity().execute();


        new FetchDailyCalories().execute();
        new FetchCaloriesByDate().execute();
        new FetchWeeklyCalories().execute();
        new FetchCaloriesByActivity().execute();
    }

    public void onConnectionSuspended(int i) {
        if(i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
            Log.i("Google API", "Network lost!");
        } else if(i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
            Log.i("Google API", "Service disconnected");
        }
    }

    public void initGoogleClient() {
        client = new GoogleApiClient.Builder(this)
                .addApi(Fitness.HISTORY_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_OAUTH) {
            authInProgress = false;
            if(resultCode == RESULT_OK) {
                if(!client.isConnecting() && !client.isConnected()) {
                    client.connect();
                }
                Log.e("GoogleFit", "RESULT OK");
                Toast.makeText(StepCounter.this, "Result OK", Toast.LENGTH_SHORT).show();
            } else if( resultCode == RESULT_CANCELED ) {
                Log.e( "GoogleFit", "RESULT CANCELLED" );
            }
        } else {
            //request code is not REQUEST_OAUTH
            Log.e("GoogleFit", "request code: " + requestCode);
        }
    }

    public void onConnectionFailed(ConnectionResult result) {
        //print the result
        Log.i("Connection Failed", result.toString());
        if(!authInProgress) {
            try {
                authInProgress = true;
                result.startResolutionForResult( StepCounter.this, REQUEST_OAUTH );
            } catch(IntentSender.SendIntentException e ) {
            }
        } else {
            Log.e("GoogleFit", "authInProgress");
        }
    }

    //highlighting nav bottom view
    private void updateNavBar() {
        int navId = getCurrentItemId();
        Menu menu = navView.getMenu();
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            boolean checked = item.getItemId() == navId;
            if(checked) {
                item.setChecked(true);
                break;
            }
        }
    }

    private long getDailyStep() {
        long dailyStep = 0;
        DailyTotalResult result = Fitness.HistoryApi.readDailyTotal(client, DataType.TYPE_STEP_COUNT_DELTA)
                .await(5, TimeUnit.SECONDS);
        DataSet dataSet = result.getTotal();
        //this is for logging
        loggerDataSet(dataSet, "Daily Steps");
        //this is for view (return value is used to update view in ui thread)
        for (DataPoint dp : dataSet.getDataPoints()) {
            dailyStep = dp.getValue(Field.FIELD_STEPS).asInt();
        }
        return dailyStep;
    }

    private long getDailyStepByDate(String dateInput) {
        long dailyStep = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = new Date();
        try {
            date = dateFormat.parse(dateInput);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        long startTime = cal.getTimeInMillis();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 59);
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        DataReadResult result = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);

        if(result.getBuckets().size() > 0) {
            for (Bucket bucket : result.getBuckets()) {
                DataSet dataSet = bucket.getDataSets().get(0);
                for(DataPoint dp : dataSet.getDataPoints()) {
                    dailyStep = dp.getValue(Field.FIELD_STEPS).asInt();
                }
            }
        }

        return dailyStep;
    }

    private List<DataSet> getWeeklyStep() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        //get start of this week
        //start on Sun, end on Sat
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        long startTime = cal.getTimeInMillis();
        //add 7 days (minus 1 second) to the calendar to get the end of this week
        cal.add(Calendar.DAY_OF_WEEK, 7);
        cal.add(Calendar.SECOND, -1);
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        DataReadResult dataReadResult = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);

        //Used for aggregated data
        if (dataReadResult.getBuckets().size() > 0) {
            Log.i("History", "Data point:");
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    loggerDataSet(dataSet, "Weekly Steps");
                }
            }
        }
        //Used for non-aggregated data
        else if (dataReadResult.getDataSets().size() > 0) {
            for (DataSet dataSet : dataReadResult.getDataSets()) {
                loggerDataSet(dataSet, "Weekly");
            }
        }
        return dataReadResult.getDataSets();
    }

    protected void loggerDataSet(DataSet dataSet, String logTag) {
        DateFormat dateFormat = DateFormat.getDateInstance();
        DateFormat timeFormat = DateFormat.getTimeInstance();

        //log them
        for (DataPoint dp : dataSet.getDataPoints()) {
            for (Field field : dp.getDataType().getFields()) {
                Log.i(logTag, "\tDate: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS))
                        + " Value (" + field.getName() + "): " + dp.getValue(field));
            }
        }
    }

    private void getDailyStepByActivity(String dateInput) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = new Date();
        try {
            date = simpleDateFormat.parse(dateInput);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        long startTime = cal.getTimeInMillis();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 59);
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByActivitySegment(1, TimeUnit.MINUTES)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        DataReadResult result = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);

        Log.i("Testing", "Size: " + result.getBuckets().size());

        if(result.getBuckets().size() > 0) {
            for (Bucket bucket : result.getBuckets()) {
                long duration = bucket.getEndTime(TimeUnit.MILLISECONDS) - bucket.getStartTime(TimeUnit.MILLISECONDS);
                Log.i("Steps by Activity", "activity: " + bucket.getActivity() + ", duration: " + calcDuration(duration));

                DateFormat dateFormat = DateFormat.getDateInstance();
                DateFormat timeFormat = DateFormat.getTimeInstance();

                Log.e("", "\tStart: " + dateFormat.format(bucket.getStartTime(TimeUnit.MILLISECONDS)) + " "
                        + timeFormat.format(bucket.getStartTime(TimeUnit.MILLISECONDS)));
                Log.e("", "\tEnd: " + dateFormat.format(bucket.getEndTime(TimeUnit.MILLISECONDS)) + " "
                        + timeFormat.format(bucket.getEndTime(TimeUnit.MILLISECONDS)));
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    defaultLogger(dataSet, "Steps by Activity");
                }
            }
        } else if (result.getDataSets().size() > 0) {
            for (DataSet dataSet : result.getDataSets()) {
                defaultLogger(dataSet, "Steps by Activity");
            }
        }
    }

    private String calcDuration(long duration) {
        //duration must be in miliseconds
        String durationStr = "";
        durationStr += TimeUnit.MILLISECONDS.toHours(duration) + "h ";
        duration -= TimeUnit.MILLISECONDS.toHours(duration) * 60 * 60 * 1000;
        durationStr += TimeUnit.MILLISECONDS.toMinutes(duration) + "m ";
        duration -= TimeUnit.MILLISECONDS.toMinutes(duration) * 60 * 1000;
        durationStr += TimeUnit.MILLISECONDS.toSeconds(duration) + "s ";
        return durationStr;
    }

    protected void defaultLogger(DataSet dataSet, String logTag) {
        for (DataPoint dp : dataSet.getDataPoints()) {
            for(Field field : dp.getDataType().getFields()) {
                Log.i(logTag, "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));
            }
        }
    }

    private int getCurrentItemId() {
        return R.id.navigation_step_daily;
    }

    public String formatDateString(String dateInput) {
        String result = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            result = dateFormat.parse(dateInput).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


    private class FetchDailyStep extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            final long dailyStep = getDailyStep();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final TextView tv = findViewById(R.id.step_today);
                    tv.setText("Daily steps: " + dailyStep);
                }
            });
            return null;
        }
    }

    private class FetchStepByDate extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            //in the form of DD-MM-YYYY
            final long dayStep = getDailyStepByDate("2019-06-01T00:00:00Z");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final TextView tv = findViewById(R.id.step_date);
                    DateFormat dateFormat = DateFormat.getDateInstance();
                    Calendar theDate = Calendar.getInstance();
                    // month = current month - 1 (Jan == 0)
                    theDate.set(2019, 5, 01);
                    tv.setText("Steps (" + dateFormat.format(theDate.getTimeInMillis()) + "): " + dayStep);
                }
            });
            return null;
        }
    }

    private class FetchStepByActivity extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            getDailyStepByActivity("2019-06-01T00:00:00Z");
            return null;
        }
    }

    private class FetchWeeklyStep extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            getWeeklyStep();
            return null;
        }
    }

    private class FetchDailyCalories extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            final float calories = getDailyCalories();
            final String caloriesString = String.format("%.02f", calories);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final TextView tv = findViewById(R.id.caloriesView);
                    tv.setText("Daily calories burned: " + caloriesString + " kcal");


                    Log.i("Daily calories burned", ": " + caloriesString + " kcal" );
                }
            });
            return null;
        }
    }

    private class FetchWeeklyCalories extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            getWeeklyCalories();
            return null;
        }
    }
    private class FetchCaloriesByDate extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            //in the form of DD-MM-YYYY
            final float calories = getCaloriesByDate("2019-06-01T00:00:00Z");
            final String caloriesString = String.format("%.02f", calories);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final TextView tv = findViewById(R.id.caloriesViewByDate);
                    DateFormat dateFormat = DateFormat.getDateInstance();
                    Calendar theDate = Calendar.getInstance();
                    // month = current month - 1 (Jan == 0)
                    theDate.set(2019, 5, 01);
                    tv.setText("Calories By Date(" + dateFormat.format(theDate.getTimeInMillis()) + "): " + caloriesString+ " kcal");
                    Log.i("Calories By Date", "Calories (" + dateFormat.format(theDate.getTimeInMillis()) + "): " + caloriesString+ " kcal" );
                }
            });
            return null;
        }
    }
    private class FetchCaloriesByActivity extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            getDailyCaloriesByActivity("2019-06-01T00:00:00Z");
            return null;
        }
    }

    private float getDailyCalories() {
        float dailyCalories = 0;
        DailyTotalResult result = Fitness.HistoryApi.readDailyTotal(client, DataType.AGGREGATE_CALORIES_EXPENDED)
                .await(5, TimeUnit.SECONDS);


        if (result.getStatus().isSuccess()) {
            DataSet totalSet = result.getTotal();
            if (totalSet != null) {
                dailyCalories = totalSet.isEmpty()
                        ? 0
                        : totalSet.getDataPoints().get(0).getValue(Field.FIELD_CALORIES).asFloat();
            }
        } else {
            //Log.w(TAG, "There was a problem getting the calories.");
            Log.d("Google API", "There was a problem getting the calories.");
        }
        //Log.d("Google API", "Calories burned today"+dailyCalories + " kcal");
        return dailyCalories;
    }


    private List<DataSet> getWeeklyCalories() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        //get start of this week
        //start on Sun, end on Sat
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        long startTime = cal.getTimeInMillis();
        //add 7 days (minus 1 second) to the calendar to get the end of this week
        cal.add(Calendar.DAY_OF_WEEK, 7);
        cal.add(Calendar.SECOND, -1);
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        DataReadResult dataReadResult = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);

        //Used for aggregated data
        if (dataReadResult.getBuckets().size() > 0) {
            Log.i("History", "Data point:");
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    loggerDataSet(dataSet, "Weekly Calories");
                }
            }
        }
        //Used for non-aggregated data
        else if (dataReadResult.getDataSets().size() > 0) {
            for (DataSet dataSet : dataReadResult.getDataSets()) {
                loggerDataSet(dataSet, "Weekly");
            }
        }
        return dataReadResult.getDataSets();
    }

    private float getCaloriesByDate(String dateInput) {
        float caloriesByDate = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = new Date();
        try {
            date = dateFormat.parse(dateInput);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        long startTime = cal.getTimeInMillis();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 59);
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        DataReadResult result = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);

        if(result.getBuckets().size() > 0) {
            for (Bucket bucket : result.getBuckets()) {
                DataSet dataSet = bucket.getDataSets().get(0);
                if (dataSet != null) {
                    caloriesByDate = dataSet.isEmpty()
                            ? 0
                            : dataSet.getDataPoints().get(0).getValue(Field.FIELD_CALORIES).asFloat();
                }
            }
        }else {
            //Log.w(TAG, "There was a problem getting the calories.");
            Log.d("Google API", "There was a problem getting the calories.");
        }

        return caloriesByDate;
    }

    private void getDailyCaloriesByActivity(String dateInput) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = new Date();
        try {
            date = simpleDateFormat.parse(dateInput);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        long startTime = cal.getTimeInMillis();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 59);
        long endTime = cal.getTimeInMillis();

        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByActivitySegment(1, TimeUnit.MINUTES)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        DataReadResult result = Fitness.HistoryApi.readData(client, readRequest).await(1, TimeUnit.MINUTES);

        Log.i("Testing", "Size: " + result.getBuckets().size());

        if(result.getBuckets().size() > 0) {
            for (Bucket bucket : result.getBuckets()) {
                long duration = bucket.getEndTime(TimeUnit.MILLISECONDS) - bucket.getStartTime(TimeUnit.MILLISECONDS);
                Log.i("Calories by Activity", "activity: " + bucket.getActivity() + ", duration: " + calcDuration(duration));

                DateFormat dateFormat = DateFormat.getDateInstance();
                DateFormat timeFormat = DateFormat.getTimeInstance();

                Log.e("", "\tStart: " + dateFormat.format(bucket.getStartTime(TimeUnit.MILLISECONDS)) + " "
                        + timeFormat.format(bucket.getStartTime(TimeUnit.MILLISECONDS)));
                Log.e("", "\tEnd: " + dateFormat.format(bucket.getEndTime(TimeUnit.MILLISECONDS)) + " "
                        + timeFormat.format(bucket.getEndTime(TimeUnit.MILLISECONDS)));
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    defaultLogger(dataSet, "Calories by Activity");
                }
            }
        } else if (result.getDataSets().size() > 0) {
            for (DataSet dataSet : result.getDataSets()) {
                defaultLogger(dataSet, "Calories by Activity");
            }
        }
    }
}
