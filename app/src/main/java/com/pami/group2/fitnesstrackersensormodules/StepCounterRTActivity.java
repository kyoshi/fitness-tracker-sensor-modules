package com.pami.group2.fitnesstrackersensormodules;

import android.content.Intent;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;

import java.util.concurrent.TimeUnit;

public class StepCounterRTActivity extends AppCompatActivity implements OnDataPointListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleClient;
    private TextView textView;
    private static final int REQUEST_OAUTH = 1;
    private static final String AUTH_PENDING = "auth_state_pending";
    private boolean authInProgress = false;
    private BottomNavigationView navView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startIntent(MainActivity.class);
                    return true;
                case R.id.navigation_step_rt:
                    return true;
                case R.id.navigation_step_daily:
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_counter_rt);
        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        updateNavBar();

        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }

        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        textView = findViewById(R.id.textView);
    }

    protected void startIntent(Class cls) {
        Intent intent = new Intent(StepCounterRTActivity.this, cls);
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTH_PENDING, authInProgress);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateNavBar();
        googleClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        Fitness.SensorsApi.remove(googleClient, this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if(status.isSuccess()) {
                            googleClient.disconnect();
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_OAUTH) {
            authInProgress = false;
            if(resultCode == RESULT_OK) {
                if(!googleClient.isConnecting() && !googleClient.isConnected()) {
                    googleClient.connect();
                }
                Log.e("GoogleFit", "RESULT OK");
                Toast.makeText(StepCounterRTActivity.this, "Result OK", Toast.LENGTH_SHORT).show();
            } else if( resultCode == RESULT_CANCELED ) {
                Log.e( "GoogleFit", "RESULT CANCELLED" );
            }
        } else {
            //request code is not REQUEST_OAUTH
            Log.e("GoogleFit", "request code: " + requestCode);
        }
    }

    //callback when sensors are connected
    @Override
    public void onConnected(Bundle bundle) {
        DataSourcesRequest request = new DataSourcesRequest.Builder()
                .setDataTypes(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .setDataSourceTypes(DataSource.TYPE_RAW)
                .build();

        ResultCallback<DataSourcesResult> resultCallback = new ResultCallback<DataSourcesResult>() {
            @Override
            public void onResult(DataSourcesResult dataSourcesResult) {
                for(DataSource dataSource : dataSourcesResult.getDataSources()) {
                    if(dataSource.getDataType().equals(DataType.TYPE_STEP_COUNT_CUMULATIVE)) {
                        registerFitnessDataListener(dataSource, DataType.TYPE_STEP_COUNT_CUMULATIVE);
                    }
                }
            }
        };

        Fitness.SensorsApi.findDataSources(googleClient, request)
                .setResultCallback(resultCallback);
    }

    private void registerFitnessDataListener(DataSource dataSource, DataType dataType) {
        SensorRequest request = new SensorRequest.Builder()
                .setDataSource(dataSource)
                .setDataType(dataType)
                .setSamplingRate(1, TimeUnit.SECONDS)
                .build();

        Fitness.SensorsApi.add(googleClient, request, this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if(status.isSuccess()) {
                            Log.e( "GoogleFit", "SensorApi successfully added" );
                        }
                    }
                });
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if(!authInProgress) {
            try {
                authInProgress = true;
                result.startResolutionForResult( StepCounterRTActivity.this, REQUEST_OAUTH );
            } catch(IntentSender.SendIntentException e ) {
            }
        } else {
            Log.e("GoogleFit", "authInProgress");
        }
    }

    @Override
    public void onDataPoint(DataPoint dataPoint) {
        for(Field f : dataPoint.getDataType().getFields()) {
            final Value stepValue = dataPoint.getValue(f);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText("Current step: " + stepValue);
                }
            });
        }
    }

    //highlighting nav bottom view
    private void updateNavBar() {
        int navId = getCurrentItemId();
        Menu menu = navView.getMenu();
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            boolean checked = item.getItemId() == navId;
            if(checked) {
                item.setChecked(true);
                break;
            }
        }
    }

    private int getCurrentItemId() {
        return R.id.navigation_step_rt;
    }
}
