package com.pami.group2.fitnesstrackersensormodules.google_api_service;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataSourcesRequest;

public class GoogleApiConnect implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient client;
    private Context ctx;
    private Api apiType;
    private DataType dataType;

    public GoogleApiConnect(Context actContext, String apiTypeString) {
        ctx = actContext;
        apiType = this.toGoogleApi(apiTypeString);
        this.setDataType();
        this.build();
    }

    // GETTER METHODS
    public GoogleApiClient getClient() {
        return this.client;
    }
    public Context getContext() {
        return this.ctx;
    }

    //SETTER
    public void setContext(Context c) {
        this.ctx = c;
    }

    private void build() {
        client = new GoogleApiClient.Builder(this.ctx)
                .addApi(apiType)
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private Api toGoogleApi(String s) {
        switch (s) {
            case "sensors_api":
                return Fitness.SENSORS_API;
            case "history_api":
                return Fitness.HISTORY_API;
            default:
                return null;
        }
    }

    private void setDataType() {
        if (this.apiType == Fitness.SENSORS_API) {
            dataType = DataType.TYPE_STEP_COUNT_CUMULATIVE;
        }
    }

    public DataSourcesRequest buildDataSourceRequest() {
        DataSourcesRequest request = new DataSourcesRequest.Builder()
                .setDataTypes(this.dataType)
                .setDataSourceTypes(DataSource.TYPE_RAW)
                .build();
        return request;
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
    }
}
