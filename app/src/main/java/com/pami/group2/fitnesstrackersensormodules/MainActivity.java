package com.pami.group2.fitnesstrackersensormodules;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private TextView mTextMessage;
    private GoogleApiClient client;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_step_rt:
                    startIntent(StepCounterRTActivity.class);
                    return true;
                case R.id.navigation_step_daily:
                    startIntent(StepCounter.class);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initGoogleClient();

        Button profileBtn  = (Button)findViewById(R.id.profileBtn);

        profileBtn.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(MainActivity.this, DatabasesQueries.class);
                myIntent.putExtra("key", "123"); //Optional parameters
                MainActivity.this.startActivity(myIntent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        client.connect();
        // for logging and debugging, to show list of subscriptions the app has
        // showSubscriptions();
    }

    public void initGoogleClient() {
        client = new GoogleApiClient.Builder(this)
                .addApi(Fitness.RECORDING_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void showSubscriptions() {
        Fitness.RecordingApi.listSubscriptions(client)
                .setResultCallback(new ResultCallback<ListSubscriptionsResult>() {
                    @Override
                    public void onResult(@NonNull ListSubscriptionsResult result) {
                        for (Subscription subscription : result.getSubscriptions()) {
                            DataType dataType = subscription.getDataType();
                            Log.e( "RecordingAPI", dataType.getName() );
                            for (Field field : dataType.getFields() ) {
                                Log.e( "RecordingAPI", field.toString() );
                            }
                        }
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConnected(Bundle bundle) {
        subscribeTo(DataType.TYPE_STEP_COUNT_DELTA);
        subscribeTo(DataType.TYPE_CALORIES_EXPENDED);
    }

    private void subscribeTo(final DataType dataType) {
        Fitness.RecordingApi.subscribe(client, dataType)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if(status.isSuccess()) {
                            if(status.getStatusCode() == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                                Log.i("Subscription", "Already subscribed to: " + dataType.getName());
                            } else {
                                Log.i("Subscription", "Subscribed to " + dataType.getName());
                            }
                        } else {
                            Log.e("Subscription", "Problem with subscribing");
                        }
                    }
                });
    }

    @Override
    public void onConnectionSuspended(int i) {
        if(i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
            Log.i("Google API", "Network lost!");
        } else if(i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
            Log.i("Google API", "Service disconnected");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("Error", "Connection Failed");
        Log.e("Error", "");
    }

    protected void startIntent(Class cls) {
        Intent intent = new Intent(MainActivity.this, cls);
        startActivity(intent);
    }
}
